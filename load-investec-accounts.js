// load credentials from .env file
require('dotenv').config();
const INVESTEC_CLIENT_ID = process.env.INVESTEC_CLIENT_ID;
const INVESTEC_SECRET = process.env.INVESTEC_SECRET;

if (!INVESTEC_CLIENT_ID || INVESTEC_CLIENT_ID.length == 0) {
    console.log(`investec credentials not found, aborting`);
    process.exit(1);
}

base64 = require('base-64');
const fetch = require('node-fetch');
const fs = require('fs');

let accountsConfig;
try {
    accountsConfig = JSON.parse(fs.readFileSync('./accounts.json', 'utf8'));
} catch (e) {
    fs.writeFileSync('./accounts.json', '{}');
    console.log(`accounts.json created`);
    accountsConfig = JSON.parse(fs.readFileSync('./accounts.json', 'utf8'));
}

let investecOpenApi = 'https://openapi.investec.com';

// get auth token
fetch(`${investecOpenApi}/identity/v2/oauth2/token`, {
    method: 'post',
    body:    'grant_type=client_credentials&scope=accounts',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': `Basic ${base64.encode(INVESTEC_CLIENT_ID + ":" + INVESTEC_SECRET)}`
    },
})
.then(res => res.json())
.then((authResponse) => {
    console.log(`auth success: ${JSON.stringify(authResponse)}`);
    let token = authResponse.access_token;

    fetch(`${investecOpenApi}/za/pb/v1/accounts`, {
        headers: {
            'Authorization': `Bearer ${token}`
        },
    })
    .then(res => res.json())
    .then(response => {
        let accounts = response.data.accounts;
        for (let i in accounts) {
            let account = accounts[i];
            account.bank = "investec";
            let original = accountsConfig[account.accountId];
            if (original && original.budgieAccountId) {
                // retain budgieAccountId and last upload date
                account.budgieAccountId = original.budgieAccountId;
                account.lastUpload = original.lastUpload;
            }
            accountsConfig[account.accountId] = account;
        }
        console.log(accountsConfig);
        fs.writeFileSync('./accounts.json', JSON.stringify(accountsConfig, null, 4));
        console.log(`./accounts.json updated, please add "budgieAccountId" fields as required`);
    });
});
