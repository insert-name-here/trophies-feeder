# trophies-feeder

Assign funds to a [trophies](https://gitlab.com/insert-name-here/trophies) project based on a transaction description.
This project is based on and uses logic from the [budgie-feeder](https://gitlab.com/insert-name-here/budgie-feeder) project.

## Prerequisites

Run `npm install` to install script dependencies.

## Sensitive Data

Sensitive data (eg. credentials, account numbers) are to be stored in a `.env` file that must never be checked in. Copy the `sensitives-template` file to `.env` and complete it manually.

## Account Configuration

The `accounts.json` configuration file will be created when adding or loading accounts.

### Investec Accounts

To load Investec accounts into the configuration file, run `node load-investec-accounts.js`.
## Operation

#### Import Investec Transactions

To check the latest transactions since the last run and assign funds to a project, run `node import-investec.js`.
