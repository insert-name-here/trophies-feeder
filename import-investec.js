// load credentials from .env file
require('dotenv').config();
const INVESTEC_CLIENT_ID = process.env.INVESTEC_CLIENT_ID;
const INVESTEC_SECRET = process.env.INVESTEC_SECRET;
const TROPHY_PROJECT_ID = process.env.TROPHY_PROJECT_ID;
const TROPHY_PROJECT_FUNDING_KEY = process.env.TROPHY_PROJECT_FUNDING_KEY;
const TROPHY_API_BASE_ENDPOINT =  process.env.TROPHY_API_BASE_ENDPOINT;

if (!INVESTEC_CLIENT_ID || INVESTEC_CLIENT_ID.length == 0) {
    console.log(`investec credentials not found, aborting`);
    process.exit(1);
}

let accountsConfig = require('./accounts.json');

base64 = require('base-64');
const fetch = require('node-fetch');
const fs = require('fs');
const moment = require('moment');

let investecOpenApi = 'https://openapi.investec.com';

let convertAmount = (investecAmount) => {
    return Math.round(investecAmount);
}

// get auth token
fetch(`${investecOpenApi}/identity/v2/oauth2/token`, {
    method: 'post',
    body:    'grant_type=client_credentials&scope=accounts',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': `Basic ${base64.encode(INVESTEC_CLIENT_ID + ":" + INVESTEC_SECRET)}`
    },
})
.then(res => res.json())
.then((authResponse) => {
    console.log(`auth success: ${JSON.stringify(authResponse)}`);
    let token = authResponse.access_token;

    // download the transactions since the latest date
    for (let accountId in accountsConfig) {
        let account = accountsConfig[accountId];
        if (account.bank != "investec") {
            console.log(`${accountId} is not an investec account, skipping.`);
            continue;
        }
        console.log(`downloading transactions from ${accountId}...`);
        let fromDate = account.lastUpload ? `?fromDate=${account.lastUpload}` : '';

        // TODO: what happens if a transaction if from a few days prior but the posting date is
        //      today? will today's fromDate pick up the transaction?
        fetch(`${investecOpenApi}/za/pb/v1/accounts/${accountId}/transactions${fromDate}`, {
            headers: {
                'Authorization': `Bearer ${token}`
            },
        })
        .then(res => res.json())
        .then(response => {
            let transactions = response.data.transactions;     
            for (let t in transactions) {
                let transaction = transactions[t];                      
                if(transaction.description.toUpperCase().indexOf(TROPHY_PROJECT_ID.toUpperCase()) !== -1){
                    //POST to the trophy project funding
                    let fundingAmount = convertAmount(transaction.amount);
                    let funding = {
                                    funds: fundingAmount,
                                    source: transaction.description                    
                                  }
                    //let fundingApi = TROPHY_API_BASE_ENDPOINT+'/projects/118bdc4e-e6a1-4e67-9c95-cfbc1b3cba71/funds';
                    let fundingApi = TROPHY_API_BASE_ENDPOINT+'/projects/'+TROPHY_PROJECT_ID+'/funds';
                    let postBody = JSON.stringify(funding);
                    console.log(`Sending ${fundingAmount} from ${transaction.description} transaction to projectID ${TROPHY_PROJECT_ID} ...`)
                    fetch(fundingApi, {
                        method: 'post',
                        body:    postBody,
                        headers: {
                            'apiKey': TROPHY_PROJECT_FUNDING_KEY,
                            'Content-Type': 'application/json',
                        },
                    })
                    .then(res => res.json())
                    .then((response) => {
                        console.log(JSON.stringify(response, null, 4));                        
                    });

                }
            }
            // reload accounts config
            accountsConfig = require('./accounts.json');
            // update last upload date
            accountsConfig[accountId].lastUpload =  moment().format('YYYY-MM-DD');
            // rewrite accounts config
            fs.writeFileSync('./accounts.json', JSON.stringify(accountsConfig, null, 4));
        });
    }
});




